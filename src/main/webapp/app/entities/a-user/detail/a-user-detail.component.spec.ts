import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AUserDetailComponent } from './a-user-detail.component';

describe('AUser Management Detail Component', () => {
  let comp: AUserDetailComponent;
  let fixture: ComponentFixture<AUserDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AUserDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ aUser: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AUserDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AUserDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load aUser on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.aUser).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
