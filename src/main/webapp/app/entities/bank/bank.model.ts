import { IDelivery } from 'app/entities/delivery/delivery.model';

export interface IBank {
  id?: number;
  streetAddress?: string | null;
  postalCode?: string | null;
  city?: string | null;
  stateProvince?: string | null;
  owns?: IDelivery | null;
}

export class Bank implements IBank {
  constructor(
    public id?: number,
    public streetAddress?: string | null,
    public postalCode?: string | null,
    public city?: string | null,
    public stateProvince?: string | null,
    public owns?: IDelivery | null
  ) {}
}

export function getBankIdentifier(bank: IBank): number | undefined {
  return bank.id;
}
