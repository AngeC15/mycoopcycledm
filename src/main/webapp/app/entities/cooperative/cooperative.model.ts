import dayjs from 'dayjs/esm';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { IAUser } from 'app/entities/a-user/a-user.model';

export interface ICooperative {
  id?: number;
  name?: string;
  creationDate?: dayjs.Dayjs | null;
  nameDG?: string | null;
  have?: IRestaurant[] | null;
  members?: IAUser[] | null;
  leaders?: IAUser[] | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public name?: string,
    public creationDate?: dayjs.Dayjs | null,
    public nameDG?: string | null,
    public have?: IRestaurant[] | null,
    public members?: IAUser[] | null,
    public leaders?: IAUser[] | null
  ) {}
}

export function getCooperativeIdentifier(cooperative: ICooperative): number | undefined {
  return cooperative.id;
}
