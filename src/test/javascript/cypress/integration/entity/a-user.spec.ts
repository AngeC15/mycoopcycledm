import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('AUser e2e test', () => {
  const aUserPageUrl = '/a-user';
  const aUserPageUrlPattern = new RegExp('/a-user(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const aUserSample = {};

  let aUser: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/a-users+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/a-users').as('postEntityRequest');
    cy.intercept('DELETE', '/api/a-users/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (aUser) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/a-users/${aUser.id}`,
      }).then(() => {
        aUser = undefined;
      });
    }
  });

  it('AUsers menu should load AUsers page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('a-user');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('AUser').should('exist');
    cy.url().should('match', aUserPageUrlPattern);
  });

  describe('AUser page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(aUserPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create AUser page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/a-user/new$'));
        cy.getEntityCreateUpdateHeading('AUser');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aUserPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/a-users',
          body: aUserSample,
        }).then(({ body }) => {
          aUser = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/a-users+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [aUser],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(aUserPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details AUser page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('aUser');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aUserPageUrlPattern);
      });

      it('edit button click should load edit AUser page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('AUser');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aUserPageUrlPattern);
      });

      it('last delete button click should delete instance of AUser', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('aUser').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aUserPageUrlPattern);

        aUser = undefined;
      });
    });
  });

  describe('new AUser page', () => {
    beforeEach(() => {
      cy.visit(`${aUserPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('AUser');
    });

    it('should create an instance of AUser', () => {
      cy.get(`[data-cy="name"]`).type('bottom-line Iowa Rubber').should('have.value', 'bottom-line Iowa Rubber');

      cy.get(`[data-cy="surname"]`).type('grow').should('have.value', 'grow');

      cy.get(`[data-cy="email"]`).type('Jonatan.Gibson@yahoo.com').should('have.value', 'Jonatan.Gibson@yahoo.com');

      cy.get(`[data-cy="phoneNumber"]`).type('0033519-566000').should('have.value', '0033519-566000');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        aUser = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', aUserPageUrlPattern);
    });
  });
});
