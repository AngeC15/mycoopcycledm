package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.AUser;
import info4.gl.blog.domain.Delivery;
import info4.gl.blog.service.dto.AUserDTO;
import info4.gl.blog.service.dto.DeliveryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Delivery} and its DTO {@link DeliveryDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryMapper extends EntityMapper<DeliveryDTO, Delivery> {
    @Mapping(target = "creator", source = "creator", qualifiedByName = "aUserId")
    DeliveryDTO toDto(Delivery s);

    @Named("aUserId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AUserDTO toDtoAUserId(AUser aUser);
}
