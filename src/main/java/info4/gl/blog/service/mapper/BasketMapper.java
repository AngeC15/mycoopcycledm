package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Basket;
import info4.gl.blog.domain.Delivery;
import info4.gl.blog.domain.Products;
import info4.gl.blog.service.dto.BasketDTO;
import info4.gl.blog.service.dto.DeliveryDTO;
import info4.gl.blog.service.dto.ProductsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Basket} and its DTO {@link BasketDTO}.
 */
@Mapper(componentModel = "spring")
public interface BasketMapper extends EntityMapper<BasketDTO, Basket> {
    @Mapping(target = "delivery", source = "delivery", qualifiedByName = "deliveryId")
    @Mapping(target = "choosen", source = "choosen", qualifiedByName = "productsId")
    BasketDTO toDto(Basket s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);

    @Named("productsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductsDTO toDtoProductsId(Products products);
}
