package info4.gl.blog.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import info4.gl.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AUserDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AUserDTO.class);
        AUserDTO aUserDTO1 = new AUserDTO();
        aUserDTO1.setId(1L);
        AUserDTO aUserDTO2 = new AUserDTO();
        assertThat(aUserDTO1).isNotEqualTo(aUserDTO2);
        aUserDTO2.setId(aUserDTO1.getId());
        assertThat(aUserDTO1).isEqualTo(aUserDTO2);
        aUserDTO2.setId(2L);
        assertThat(aUserDTO1).isNotEqualTo(aUserDTO2);
        aUserDTO1.setId(null);
        assertThat(aUserDTO1).isNotEqualTo(aUserDTO2);
    }
}
