import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { IBasket } from 'app/entities/basket/basket.model';
import { IDelivery } from 'app/entities/delivery/delivery.model';

export interface IAUser {
  id?: number;
  name?: string | null;
  surname?: string | null;
  email?: string | null;
  phoneNumber?: string | null;
  possesses?: IRestaurant | null;
  restaurant?: IRestaurant | null;
  areIn?: ICooperative | null;
  lead?: ICooperative | null;
  own?: IBasket | null;
  creations?: IDelivery[] | null;
}

export class AUser implements IAUser {
  constructor(
    public id?: number,
    public name?: string | null,
    public surname?: string | null,
    public email?: string | null,
    public phoneNumber?: string | null,
    public possesses?: IRestaurant | null,
    public restaurant?: IRestaurant | null,
    public areIn?: ICooperative | null,
    public lead?: ICooperative | null,
    public own?: IBasket | null,
    public creations?: IDelivery[] | null
  ) {}
}

export function getAUserIdentifier(aUser: IAUser): number | undefined {
  return aUser.id;
}
