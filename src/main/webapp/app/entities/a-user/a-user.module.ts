import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AUserComponent } from './list/a-user.component';
import { AUserDetailComponent } from './detail/a-user-detail.component';
import { AUserUpdateComponent } from './update/a-user-update.component';
import { AUserDeleteDialogComponent } from './delete/a-user-delete-dialog.component';
import { AUserRoutingModule } from './route/a-user-routing.module';

@NgModule({
  imports: [SharedModule, AUserRoutingModule],
  declarations: [AUserComponent, AUserDetailComponent, AUserUpdateComponent, AUserDeleteDialogComponent],
  entryComponents: [AUserDeleteDialogComponent],
})
export class AUserModule {}
