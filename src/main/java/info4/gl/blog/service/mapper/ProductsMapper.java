package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Delivery;
import info4.gl.blog.domain.Products;
import info4.gl.blog.service.dto.DeliveryDTO;
import info4.gl.blog.service.dto.ProductsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Products} and its DTO {@link ProductsDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductsMapper extends EntityMapper<ProductsDTO, Products> {
    @Mapping(target = "areIn", source = "areIn", qualifiedByName = "deliveryId")
    ProductsDTO toDto(Products s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);
}
