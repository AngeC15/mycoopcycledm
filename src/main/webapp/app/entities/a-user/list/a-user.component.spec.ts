import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AUserService } from '../service/a-user.service';

import { AUserComponent } from './a-user.component';

describe('AUser Management Component', () => {
  let comp: AUserComponent;
  let fixture: ComponentFixture<AUserComponent>;
  let service: AUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AUserComponent],
    })
      .overrideTemplate(AUserComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AUserComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AUserService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.aUsers?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
