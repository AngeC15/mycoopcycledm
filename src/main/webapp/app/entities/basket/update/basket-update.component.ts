import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IBasket, Basket } from '../basket.model';
import { BasketService } from '../service/basket.service';
import { IDelivery } from 'app/entities/delivery/delivery.model';
import { DeliveryService } from 'app/entities/delivery/service/delivery.service';
import { IProducts } from 'app/entities/products/products.model';
import { ProductsService } from 'app/entities/products/service/products.service';

@Component({
  selector: 'jhi-basket-update',
  templateUrl: './basket-update.component.html',
})
export class BasketUpdateComponent implements OnInit {
  isSaving = false;

  deliveriesCollection: IDelivery[] = [];
  productsSharedCollection: IProducts[] = [];

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    creationDate: [],
    price: [null, [Validators.min(0)]],
    delivery: [],
    choosen: [],
  });

  constructor(
    protected basketService: BasketService,
    protected deliveryService: DeliveryService,
    protected productsService: ProductsService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ basket }) => {
      if (basket.id === undefined) {
        const today = dayjs().startOf('day');
        basket.creationDate = today;
      }

      this.updateForm(basket);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const basket = this.createFromForm();
    if (basket.id !== undefined) {
      this.subscribeToSaveResponse(this.basketService.update(basket));
    } else {
      this.subscribeToSaveResponse(this.basketService.create(basket));
    }
  }

  trackDeliveryById(_index: number, item: IDelivery): number {
    return item.id!;
  }

  trackProductsById(_index: number, item: IProducts): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBasket>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(basket: IBasket): void {
    this.editForm.patchValue({
      id: basket.id,
      creationDate: basket.creationDate ? basket.creationDate.format(DATE_TIME_FORMAT) : null,
      price: basket.price,
      delivery: basket.delivery,
      choosen: basket.choosen,
    });

    this.deliveriesCollection = this.deliveryService.addDeliveryToCollectionIfMissing(this.deliveriesCollection, basket.delivery);
    this.productsSharedCollection = this.productsService.addProductsToCollectionIfMissing(this.productsSharedCollection, basket.choosen);
  }

  protected loadRelationshipsOptions(): void {
    this.deliveryService
      .query({ filter: 'basket-is-null' })
      .pipe(map((res: HttpResponse<IDelivery[]>) => res.body ?? []))
      .pipe(
        map((deliveries: IDelivery[]) =>
          this.deliveryService.addDeliveryToCollectionIfMissing(deliveries, this.editForm.get('delivery')!.value)
        )
      )
      .subscribe((deliveries: IDelivery[]) => (this.deliveriesCollection = deliveries));

    this.productsService
      .query()
      .pipe(map((res: HttpResponse<IProducts[]>) => res.body ?? []))
      .pipe(
        map((products: IProducts[]) => this.productsService.addProductsToCollectionIfMissing(products, this.editForm.get('choosen')!.value))
      )
      .subscribe((products: IProducts[]) => (this.productsSharedCollection = products));
  }

  protected createFromForm(): IBasket {
    return {
      ...new Basket(),
      id: this.editForm.get(['id'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value
        ? dayjs(this.editForm.get(['creationDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      price: this.editForm.get(['price'])!.value,
      delivery: this.editForm.get(['delivery'])!.value,
      choosen: this.editForm.get(['choosen'])!.value,
    };
  }
}
