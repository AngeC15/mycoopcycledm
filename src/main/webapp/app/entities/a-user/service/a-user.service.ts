import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAUser, getAUserIdentifier } from '../a-user.model';

export type EntityResponseType = HttpResponse<IAUser>;
export type EntityArrayResponseType = HttpResponse<IAUser[]>;

@Injectable({ providedIn: 'root' })
export class AUserService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/a-users');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(aUser: IAUser): Observable<EntityResponseType> {
    return this.http.post<IAUser>(this.resourceUrl, aUser, { observe: 'response' });
  }

  update(aUser: IAUser): Observable<EntityResponseType> {
    return this.http.put<IAUser>(`${this.resourceUrl}/${getAUserIdentifier(aUser) as number}`, aUser, { observe: 'response' });
  }

  partialUpdate(aUser: IAUser): Observable<EntityResponseType> {
    return this.http.patch<IAUser>(`${this.resourceUrl}/${getAUserIdentifier(aUser) as number}`, aUser, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAUser>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAUserToCollectionIfMissing(aUserCollection: IAUser[], ...aUsersToCheck: (IAUser | null | undefined)[]): IAUser[] {
    const aUsers: IAUser[] = aUsersToCheck.filter(isPresent);
    if (aUsers.length > 0) {
      const aUserCollectionIdentifiers = aUserCollection.map(aUserItem => getAUserIdentifier(aUserItem)!);
      const aUsersToAdd = aUsers.filter(aUserItem => {
        const aUserIdentifier = getAUserIdentifier(aUserItem);
        if (aUserIdentifier == null || aUserCollectionIdentifiers.includes(aUserIdentifier)) {
          return false;
        }
        aUserCollectionIdentifiers.push(aUserIdentifier);
        return true;
      });
      return [...aUsersToAdd, ...aUserCollection];
    }
    return aUserCollection;
  }
}
