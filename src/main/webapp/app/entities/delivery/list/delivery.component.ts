import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDelivery } from '../delivery.model';
import { DeliveryService } from '../service/delivery.service';
import { DeliveryDeleteDialogComponent } from '../delete/delivery-delete-dialog.component';

@Component({
  selector: 'jhi-delivery',
  templateUrl: './delivery.component.html',
})
export class DeliveryComponent implements OnInit {
  deliveries?: IDelivery[];
  isLoading = false;

  constructor(protected deliveryService: DeliveryService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.deliveryService.query().subscribe({
      next: (res: HttpResponse<IDelivery[]>) => {
        this.isLoading = false;
        this.deliveries = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IDelivery): number {
    return item.id!;
  }

  delete(delivery: IDelivery): void {
    const modalRef = this.modalService.open(DeliveryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.delivery = delivery;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
