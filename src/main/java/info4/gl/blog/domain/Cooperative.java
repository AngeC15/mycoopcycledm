package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Cooperative.
 */
@Entity
@Table(name = "cooperative")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Cooperative implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "creation_date")
    private Instant creationDate;

    @Column(name = "name_dg")
    private String nameDG;

    @OneToMany(mappedBy = "belongs")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "offer", "aUsers", "belongs", "owners" }, allowSetters = true)
    private Set<Restaurant> have = new HashSet<>();

    @OneToMany(mappedBy = "areIn")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private Set<AUser> members = new HashSet<>();

    @OneToMany(mappedBy = "lead")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "possesses", "restaurant", "areIn", "lead", "own", "creations" }, allowSetters = true)
    private Set<AUser> leaders = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Cooperative id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Cooperative name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreationDate() {
        return this.creationDate;
    }

    public Cooperative creationDate(Instant creationDate) {
        this.setCreationDate(creationDate);
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getNameDG() {
        return this.nameDG;
    }

    public Cooperative nameDG(String nameDG) {
        this.setNameDG(nameDG);
        return this;
    }

    public void setNameDG(String nameDG) {
        this.nameDG = nameDG;
    }

    public Set<Restaurant> getHave() {
        return this.have;
    }

    public void setHave(Set<Restaurant> restaurants) {
        if (this.have != null) {
            this.have.forEach(i -> i.setBelongs(null));
        }
        if (restaurants != null) {
            restaurants.forEach(i -> i.setBelongs(this));
        }
        this.have = restaurants;
    }

    public Cooperative have(Set<Restaurant> restaurants) {
        this.setHave(restaurants);
        return this;
    }

    public Cooperative addHas(Restaurant restaurant) {
        this.have.add(restaurant);
        restaurant.setBelongs(this);
        return this;
    }

    public Cooperative removeHas(Restaurant restaurant) {
        this.have.remove(restaurant);
        restaurant.setBelongs(null);
        return this;
    }

    public Set<AUser> getMembers() {
        return this.members;
    }

    public void setMembers(Set<AUser> aUsers) {
        if (this.members != null) {
            this.members.forEach(i -> i.setAreIn(null));
        }
        if (aUsers != null) {
            aUsers.forEach(i -> i.setAreIn(this));
        }
        this.members = aUsers;
    }

    public Cooperative members(Set<AUser> aUsers) {
        this.setMembers(aUsers);
        return this;
    }

    public Cooperative addMember(AUser aUser) {
        this.members.add(aUser);
        aUser.setAreIn(this);
        return this;
    }

    public Cooperative removeMember(AUser aUser) {
        this.members.remove(aUser);
        aUser.setAreIn(null);
        return this;
    }

    public Set<AUser> getLeaders() {
        return this.leaders;
    }

    public void setLeaders(Set<AUser> aUsers) {
        if (this.leaders != null) {
            this.leaders.forEach(i -> i.setLead(null));
        }
        if (aUsers != null) {
            aUsers.forEach(i -> i.setLead(this));
        }
        this.leaders = aUsers;
    }

    public Cooperative leaders(Set<AUser> aUsers) {
        this.setLeaders(aUsers);
        return this;
    }

    public Cooperative addLeader(AUser aUser) {
        this.leaders.add(aUser);
        aUser.setLead(this);
        return this;
    }

    public Cooperative removeLeader(AUser aUser) {
        this.leaders.remove(aUser);
        aUser.setLead(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cooperative)) {
            return false;
        }
        return id != null && id.equals(((Cooperative) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cooperative{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", nameDG='" + getNameDG() + "'" +
            "}";
    }
}
