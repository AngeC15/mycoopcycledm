package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Bank;
import info4.gl.blog.domain.Delivery;
import info4.gl.blog.service.dto.BankDTO;
import info4.gl.blog.service.dto.DeliveryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bank} and its DTO {@link BankDTO}.
 */
@Mapper(componentModel = "spring")
public interface BankMapper extends EntityMapper<BankDTO, Bank> {
    @Mapping(target = "owns", source = "owns", qualifiedByName = "deliveryId")
    BankDTO toDto(Bank s);

    @Named("deliveryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryDTO toDtoDeliveryId(Delivery delivery);
}
