import { IBasket } from 'app/entities/basket/basket.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { IDelivery } from 'app/entities/delivery/delivery.model';

export interface IProducts {
  id?: number;
  name?: string;
  price?: number;
  piece?: number | null;
  isIns?: IBasket[] | null;
  isAvailableIn?: IRestaurant | null;
  areIn?: IDelivery | null;
}

export class Products implements IProducts {
  constructor(
    public id?: number,
    public name?: string,
    public price?: number,
    public piece?: number | null,
    public isIns?: IBasket[] | null,
    public isAvailableIn?: IRestaurant | null,
    public areIn?: IDelivery | null
  ) {}
}

export function getProductsIdentifier(products: IProducts): number | undefined {
  return products.id;
}
