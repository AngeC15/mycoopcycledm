import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AUserComponent } from '../list/a-user.component';
import { AUserDetailComponent } from '../detail/a-user-detail.component';
import { AUserUpdateComponent } from '../update/a-user-update.component';
import { AUserRoutingResolveService } from './a-user-routing-resolve.service';

const aUserRoute: Routes = [
  {
    path: '',
    component: AUserComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AUserDetailComponent,
    resolve: {
      aUser: AUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AUserUpdateComponent,
    resolve: {
      aUser: AUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AUserUpdateComponent,
    resolve: {
      aUser: AUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(aUserRoute)],
  exports: [RouterModule],
})
export class AUserRoutingModule {}
