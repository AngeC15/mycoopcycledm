import { IProducts } from 'app/entities/products/products.model';
import { IAUser } from 'app/entities/a-user/a-user.model';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';

export interface IRestaurant {
  id?: number;
  streetAddress?: string | null;
  postalCode?: string | null;
  city?: string | null;
  stateProvince?: string | null;
  offer?: IProducts | null;
  aUsers?: IAUser[] | null;
  belongs?: ICooperative | null;
  owners?: IAUser[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(
    public id?: number,
    public streetAddress?: string | null,
    public postalCode?: string | null,
    public city?: string | null,
    public stateProvince?: string | null,
    public offer?: IProducts | null,
    public aUsers?: IAUser[] | null,
    public belongs?: ICooperative | null,
    public owners?: IAUser[] | null
  ) {}
}

export function getRestaurantIdentifier(restaurant: IRestaurant): number | undefined {
  return restaurant.id;
}
