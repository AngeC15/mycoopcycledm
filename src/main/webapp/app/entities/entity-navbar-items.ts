export const EntityNavbarItems = [
  {
    name: 'AUser',
    route: 'a-user',
    translationKey: 'global.menu.entities.aUser',
  },
  {
    name: 'Products',
    route: 'products',
    translationKey: 'global.menu.entities.products',
  },
  {
    name: 'Restaurant',
    route: 'restaurant',
    translationKey: 'global.menu.entities.restaurant',
  },
  {
    name: 'Cooperative',
    route: 'cooperative',
    translationKey: 'global.menu.entities.cooperative',
  },
  {
    name: 'Basket',
    route: 'basket',
    translationKey: 'global.menu.entities.basket',
  },
  {
    name: 'Bank',
    route: 'bank',
    translationKey: 'global.menu.entities.bank',
  },
  {
    name: 'Delivery',
    route: 'delivery',
    translationKey: 'global.menu.entities.delivery',
  },
];
