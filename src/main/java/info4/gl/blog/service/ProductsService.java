package info4.gl.blog.service;

import info4.gl.blog.service.dto.ProductsDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link info4.gl.blog.domain.Products}.
 */
public interface ProductsService {
    /**
     * Save a products.
     *
     * @param productsDTO the entity to save.
     * @return the persisted entity.
     */
    ProductsDTO save(ProductsDTO productsDTO);

    /**
     * Updates a products.
     *
     * @param productsDTO the entity to update.
     * @return the persisted entity.
     */
    ProductsDTO update(ProductsDTO productsDTO);

    /**
     * Partially updates a products.
     *
     * @param productsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProductsDTO> partialUpdate(ProductsDTO productsDTO);

    /**
     * Get all the products.
     *
     * @return the list of entities.
     */
    List<ProductsDTO> findAll();
    /**
     * Get all the ProductsDTO where IsAvailableIn is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<ProductsDTO> findAllWhereIsAvailableInIsNull();

    /**
     * Get the "id" products.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductsDTO> findOne(Long id);

    /**
     * Delete the "id" products.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
