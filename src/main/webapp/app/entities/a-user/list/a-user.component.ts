import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAUser } from '../a-user.model';
import { AUserService } from '../service/a-user.service';
import { AUserDeleteDialogComponent } from '../delete/a-user-delete-dialog.component';

@Component({
  selector: 'jhi-a-user',
  templateUrl: './a-user.component.html',
})
export class AUserComponent implements OnInit {
  aUsers?: IAUser[];
  isLoading = false;

  constructor(protected aUserService: AUserService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.aUserService.query().subscribe({
      next: (res: HttpResponse<IAUser[]>) => {
        this.isLoading = false;
        this.aUsers = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IAUser): number {
    return item.id!;
  }

  delete(aUser: IAUser): void {
    const modalRef = this.modalService.open(AUserDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.aUser = aUser;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
