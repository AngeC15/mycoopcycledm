import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAUser, AUser } from '../a-user.model';
import { AUserService } from '../service/a-user.service';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant/service/restaurant.service';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';
import { IBasket } from 'app/entities/basket/basket.model';
import { BasketService } from 'app/entities/basket/service/basket.service';

@Component({
  selector: 'jhi-a-user-update',
  templateUrl: './a-user-update.component.html',
})
export class AUserUpdateComponent implements OnInit {
  isSaving = false;

  restaurantsSharedCollection: IRestaurant[] = [];
  cooperativesSharedCollection: ICooperative[] = [];
  basketsSharedCollection: IBasket[] = [];

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    name: [],
    surname: [],
    email: [],
    phoneNumber: [
      null,
      [
        Validators.pattern(
          '^(?:(?:\\+|00)33[\\s.-]{0,3}(?:\\(0\\)[\\s.-]{0,3})?|0)[1-9](?:(?:[\\s.-]?\\d{2}){4}|\\d{2}(?:[\\s.-]?\\d{3}){2})$'
        ),
      ],
    ],
    possesses: [],
    restaurant: [],
    areIn: [],
    lead: [],
    own: [],
  });

  constructor(
    protected aUserService: AUserService,
    protected restaurantService: RestaurantService,
    protected cooperativeService: CooperativeService,
    protected basketService: BasketService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aUser }) => {
      this.updateForm(aUser);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const aUser = this.createFromForm();
    if (aUser.id !== undefined) {
      this.subscribeToSaveResponse(this.aUserService.update(aUser));
    } else {
      this.subscribeToSaveResponse(this.aUserService.create(aUser));
    }
  }

  trackRestaurantById(_index: number, item: IRestaurant): number {
    return item.id!;
  }

  trackCooperativeById(_index: number, item: ICooperative): number {
    return item.id!;
  }

  trackBasketById(_index: number, item: IBasket): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAUser>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(aUser: IAUser): void {
    this.editForm.patchValue({
      id: aUser.id,
      name: aUser.name,
      surname: aUser.surname,
      email: aUser.email,
      phoneNumber: aUser.phoneNumber,
      possesses: aUser.possesses,
      restaurant: aUser.restaurant,
      areIn: aUser.areIn,
      lead: aUser.lead,
      own: aUser.own,
    });

    this.restaurantsSharedCollection = this.restaurantService.addRestaurantToCollectionIfMissing(
      this.restaurantsSharedCollection,
      aUser.possesses,
      aUser.restaurant
    );
    this.cooperativesSharedCollection = this.cooperativeService.addCooperativeToCollectionIfMissing(
      this.cooperativesSharedCollection,
      aUser.areIn,
      aUser.lead
    );
    this.basketsSharedCollection = this.basketService.addBasketToCollectionIfMissing(this.basketsSharedCollection, aUser.own);
  }

  protected loadRelationshipsOptions(): void {
    this.restaurantService
      .query()
      .pipe(map((res: HttpResponse<IRestaurant[]>) => res.body ?? []))
      .pipe(
        map((restaurants: IRestaurant[]) =>
          this.restaurantService.addRestaurantToCollectionIfMissing(
            restaurants,
            this.editForm.get('possesses')!.value,
            this.editForm.get('restaurant')!.value
          )
        )
      )
      .subscribe((restaurants: IRestaurant[]) => (this.restaurantsSharedCollection = restaurants));

    this.cooperativeService
      .query()
      .pipe(map((res: HttpResponse<ICooperative[]>) => res.body ?? []))
      .pipe(
        map((cooperatives: ICooperative[]) =>
          this.cooperativeService.addCooperativeToCollectionIfMissing(
            cooperatives,
            this.editForm.get('areIn')!.value,
            this.editForm.get('lead')!.value
          )
        )
      )
      .subscribe((cooperatives: ICooperative[]) => (this.cooperativesSharedCollection = cooperatives));

    this.basketService
      .query()
      .pipe(map((res: HttpResponse<IBasket[]>) => res.body ?? []))
      .pipe(map((baskets: IBasket[]) => this.basketService.addBasketToCollectionIfMissing(baskets, this.editForm.get('own')!.value)))
      .subscribe((baskets: IBasket[]) => (this.basketsSharedCollection = baskets));
  }

  protected createFromForm(): IAUser {
    return {
      ...new AUser(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      surname: this.editForm.get(['surname'])!.value,
      email: this.editForm.get(['email'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      possesses: this.editForm.get(['possesses'])!.value,
      restaurant: this.editForm.get(['restaurant'])!.value,
      areIn: this.editForm.get(['areIn'])!.value,
      lead: this.editForm.get(['lead'])!.value,
      own: this.editForm.get(['own'])!.value,
    };
  }
}
