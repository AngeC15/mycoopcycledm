import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BasketService } from '../service/basket.service';
import { IBasket, Basket } from '../basket.model';
import { IDelivery } from 'app/entities/delivery/delivery.model';
import { DeliveryService } from 'app/entities/delivery/service/delivery.service';
import { IProducts } from 'app/entities/products/products.model';
import { ProductsService } from 'app/entities/products/service/products.service';

import { BasketUpdateComponent } from './basket-update.component';

describe('Basket Management Update Component', () => {
  let comp: BasketUpdateComponent;
  let fixture: ComponentFixture<BasketUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let basketService: BasketService;
  let deliveryService: DeliveryService;
  let productsService: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BasketUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BasketUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BasketUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    basketService = TestBed.inject(BasketService);
    deliveryService = TestBed.inject(DeliveryService);
    productsService = TestBed.inject(ProductsService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call delivery query and add missing value', () => {
      const basket: IBasket = { id: 456 };
      const delivery: IDelivery = { id: 95036 };
      basket.delivery = delivery;

      const deliveryCollection: IDelivery[] = [{ id: 86344 }];
      jest.spyOn(deliveryService, 'query').mockReturnValue(of(new HttpResponse({ body: deliveryCollection })));
      const expectedCollection: IDelivery[] = [delivery, ...deliveryCollection];
      jest.spyOn(deliveryService, 'addDeliveryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      expect(deliveryService.query).toHaveBeenCalled();
      expect(deliveryService.addDeliveryToCollectionIfMissing).toHaveBeenCalledWith(deliveryCollection, delivery);
      expect(comp.deliveriesCollection).toEqual(expectedCollection);
    });

    it('Should call Products query and add missing value', () => {
      const basket: IBasket = { id: 456 };
      const choosen: IProducts = { id: 84498 };
      basket.choosen = choosen;

      const productsCollection: IProducts[] = [{ id: 91092 }];
      jest.spyOn(productsService, 'query').mockReturnValue(of(new HttpResponse({ body: productsCollection })));
      const additionalProducts = [choosen];
      const expectedCollection: IProducts[] = [...additionalProducts, ...productsCollection];
      jest.spyOn(productsService, 'addProductsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      expect(productsService.query).toHaveBeenCalled();
      expect(productsService.addProductsToCollectionIfMissing).toHaveBeenCalledWith(productsCollection, ...additionalProducts);
      expect(comp.productsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const basket: IBasket = { id: 456 };
      const delivery: IDelivery = { id: 24225 };
      basket.delivery = delivery;
      const choosen: IProducts = { id: 12880 };
      basket.choosen = choosen;

      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(basket));
      expect(comp.deliveriesCollection).toContain(delivery);
      expect(comp.productsSharedCollection).toContain(choosen);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Basket>>();
      const basket = { id: 123 };
      jest.spyOn(basketService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: basket }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(basketService.update).toHaveBeenCalledWith(basket);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Basket>>();
      const basket = new Basket();
      jest.spyOn(basketService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: basket }));
      saveSubject.complete();

      // THEN
      expect(basketService.create).toHaveBeenCalledWith(basket);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Basket>>();
      const basket = { id: 123 };
      jest.spyOn(basketService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ basket });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(basketService.update).toHaveBeenCalledWith(basket);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackDeliveryById', () => {
      it('Should return tracked Delivery primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackDeliveryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackProductsById', () => {
      it('Should return tracked Products primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProductsById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
