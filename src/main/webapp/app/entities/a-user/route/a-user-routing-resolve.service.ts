import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAUser, AUser } from '../a-user.model';
import { AUserService } from '../service/a-user.service';

@Injectable({ providedIn: 'root' })
export class AUserRoutingResolveService implements Resolve<IAUser> {
  constructor(protected service: AUserService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((aUser: HttpResponse<AUser>) => {
          if (aUser.body) {
            return of(aUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AUser());
  }
}
