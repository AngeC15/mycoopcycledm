import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAUser, AUser } from '../a-user.model';

import { AUserService } from './a-user.service';

describe('AUser Service', () => {
  let service: AUserService;
  let httpMock: HttpTestingController;
  let elemDefault: IAUser;
  let expectedResult: IAUser | IAUser[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AUserService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      surname: 'AAAAAAA',
      email: 'AAAAAAA',
      phoneNumber: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a AUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new AUser()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          surname: 'BBBBBB',
          email: 'BBBBBB',
          phoneNumber: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AUser', () => {
      const patchObject = Object.assign(
        {
          surname: 'BBBBBB',
          phoneNumber: 'BBBBBB',
        },
        new AUser()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          surname: 'BBBBBB',
          email: 'BBBBBB',
          phoneNumber: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a AUser', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAUserToCollectionIfMissing', () => {
      it('should add a AUser to an empty array', () => {
        const aUser: IAUser = { id: 123 };
        expectedResult = service.addAUserToCollectionIfMissing([], aUser);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(aUser);
      });

      it('should not add a AUser to an array that contains it', () => {
        const aUser: IAUser = { id: 123 };
        const aUserCollection: IAUser[] = [
          {
            ...aUser,
          },
          { id: 456 },
        ];
        expectedResult = service.addAUserToCollectionIfMissing(aUserCollection, aUser);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AUser to an array that doesn't contain it", () => {
        const aUser: IAUser = { id: 123 };
        const aUserCollection: IAUser[] = [{ id: 456 }];
        expectedResult = service.addAUserToCollectionIfMissing(aUserCollection, aUser);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(aUser);
      });

      it('should add only unique AUser to an array', () => {
        const aUserArray: IAUser[] = [{ id: 123 }, { id: 456 }, { id: 7546 }];
        const aUserCollection: IAUser[] = [{ id: 123 }];
        expectedResult = service.addAUserToCollectionIfMissing(aUserCollection, ...aUserArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const aUser: IAUser = { id: 123 };
        const aUser2: IAUser = { id: 456 };
        expectedResult = service.addAUserToCollectionIfMissing([], aUser, aUser2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(aUser);
        expect(expectedResult).toContain(aUser2);
      });

      it('should accept null and undefined values', () => {
        const aUser: IAUser = { id: 123 };
        expectedResult = service.addAUserToCollectionIfMissing([], null, aUser, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(aUser);
      });

      it('should return initial array if no AUser is added', () => {
        const aUserCollection: IAUser[] = [{ id: 123 }];
        expectedResult = service.addAUserToCollectionIfMissing(aUserCollection, undefined, null);
        expect(expectedResult).toEqual(aUserCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
