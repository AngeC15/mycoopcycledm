package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Products.
 */
@Entity
@Table(name = "products")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Min(value = 0L)
    @Column(name = "price", nullable = false)
    private Long price;

    @Min(value = 1L)
    @Max(value = 12L)
    @Column(name = "piece")
    private Long piece;

    @OneToMany(mappedBy = "choosen")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "delivery", "bies", "choosen" }, allowSetters = true)
    private Set<Basket> isIns = new HashSet<>();

    @JsonIgnoreProperties(value = { "offer", "aUsers", "belongs", "owners" }, allowSetters = true)
    @OneToOne(mappedBy = "offer")
    private Restaurant isAvailableIn;

    @ManyToOne
    @JsonIgnoreProperties(value = { "have", "creator", "basket", "interrogates" }, allowSetters = true)
    private Delivery areIn;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Products id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Products name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return this.price;
    }

    public Products price(Long price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPiece() {
        return this.piece;
    }

    public Products piece(Long piece) {
        this.setPiece(piece);
        return this;
    }

    public void setPiece(Long piece) {
        this.piece = piece;
    }

    public Set<Basket> getIsIns() {
        return this.isIns;
    }

    public void setIsIns(Set<Basket> baskets) {
        if (this.isIns != null) {
            this.isIns.forEach(i -> i.setChoosen(null));
        }
        if (baskets != null) {
            baskets.forEach(i -> i.setChoosen(this));
        }
        this.isIns = baskets;
    }

    public Products isIns(Set<Basket> baskets) {
        this.setIsIns(baskets);
        return this;
    }

    public Products addIsIn(Basket basket) {
        this.isIns.add(basket);
        basket.setChoosen(this);
        return this;
    }

    public Products removeIsIn(Basket basket) {
        this.isIns.remove(basket);
        basket.setChoosen(null);
        return this;
    }

    public Restaurant getIsAvailableIn() {
        return this.isAvailableIn;
    }

    public void setIsAvailableIn(Restaurant restaurant) {
        if (this.isAvailableIn != null) {
            this.isAvailableIn.setOffer(null);
        }
        if (restaurant != null) {
            restaurant.setOffer(this);
        }
        this.isAvailableIn = restaurant;
    }

    public Products isAvailableIn(Restaurant restaurant) {
        this.setIsAvailableIn(restaurant);
        return this;
    }

    public Delivery getAreIn() {
        return this.areIn;
    }

    public void setAreIn(Delivery delivery) {
        this.areIn = delivery;
    }

    public Products areIn(Delivery delivery) {
        this.setAreIn(delivery);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Products)) {
            return false;
        }
        return id != null && id.equals(((Products) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Products{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", piece=" + getPiece() +
            "}";
    }
}
