import { IProducts } from 'app/entities/products/products.model';
import { IAUser } from 'app/entities/a-user/a-user.model';
import { IBasket } from 'app/entities/basket/basket.model';
import { IBank } from 'app/entities/bank/bank.model';

export interface IDelivery {
  id?: number;
  streetAddress?: string | null;
  postalCode?: string | null;
  city?: string | null;
  stateProvince?: string | null;
  paid?: boolean | null;
  tripPrice?: number | null;
  have?: IProducts[] | null;
  creator?: IAUser | null;
  basket?: IBasket | null;
  interrogates?: IBank[] | null;
}

export class Delivery implements IDelivery {
  constructor(
    public id?: number,
    public streetAddress?: string | null,
    public postalCode?: string | null,
    public city?: string | null,
    public stateProvince?: string | null,
    public paid?: boolean | null,
    public tripPrice?: number | null,
    public have?: IProducts[] | null,
    public creator?: IAUser | null,
    public basket?: IBasket | null,
    public interrogates?: IBank[] | null
  ) {
    this.paid = this.paid ?? false;
  }
}

export function getDeliveryIdentifier(delivery: IDelivery): number | undefined {
  return delivery.id;
}
