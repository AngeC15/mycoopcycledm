
application {
  config {
    baseName blog,
    applicationType monolith,
    packageName info4.gl.blog,
    authenticationType jwt,
    prodDatabaseType postgresql,
    clientFramework angular,
    testFrameworks [cypress, gatling, cucumber]
  }
  entities *
}

entity AUser {
	id Long unique required,
	name String,
    surname String,
    email String,
	phoneNumber String pattern(/^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/),
}

entity Products {
	name String required,
    price Long required min(0),
    piece Long min(1) max(12),
}

entity Restaurant {
	id Long unique required,
    streetAddress String,
	postalCode String,
	city String,
	stateProvince String,
}

entity Cooperative {
	name String required,
	creationDate Instant,
    nameDG String,
    
}

entity Basket {
	id Long unique required,
    creationDate Instant,
    price Long min(0),
    
}

entity Bank {
	id Long unique required,
	streetAddress String,
	postalCode String,
	city String,
	stateProvince String,
}

entity Delivery {
	id Long unique required,
	streetAddress String,
	postalCode String,
	city String,
	stateProvince String,
    paid Boolean,
    tripPrice Long min(0),
}

relationship ManyToOne {
	Delivery{creator} to AUser{creation},
    Bank{owns} to Delivery{interrogate},
    AUser{possesses} to Restaurant{owner},
   
}

// defining multiple oneToOne relationships
relationship OneToOne {
	Restaurant{offer} to Products{isAvailableIn},
    Basket to Delivery,
}



relationship OneToMany {
    Restaurant to AUser,
    Cooperative{has} to Restaurant{belongs},
    Cooperative{member} to AUser{areIn},
    Cooperative{leader} to AUser{lead},
    Products{isIn} to Basket{choosen},
    Basket{by} to AUser{own},
    Delivery{has} to Products{areIn}

}

dto * with mapstruct
service * with serviceImpl



