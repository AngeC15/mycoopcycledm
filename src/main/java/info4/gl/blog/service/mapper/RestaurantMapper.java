package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Cooperative;
import info4.gl.blog.domain.Products;
import info4.gl.blog.domain.Restaurant;
import info4.gl.blog.service.dto.CooperativeDTO;
import info4.gl.blog.service.dto.ProductsDTO;
import info4.gl.blog.service.dto.RestaurantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Restaurant} and its DTO {@link RestaurantDTO}.
 */
@Mapper(componentModel = "spring")
public interface RestaurantMapper extends EntityMapper<RestaurantDTO, Restaurant> {
    @Mapping(target = "offer", source = "offer", qualifiedByName = "productsId")
    @Mapping(target = "belongs", source = "belongs", qualifiedByName = "cooperativeId")
    RestaurantDTO toDto(Restaurant s);

    @Named("productsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductsDTO toDtoProductsId(Products products);

    @Named("cooperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeDTO toDtoCooperativeId(Cooperative cooperative);
}
