import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DeliveryService } from '../service/delivery.service';
import { IDelivery, Delivery } from '../delivery.model';
import { IAUser } from 'app/entities/a-user/a-user.model';
import { AUserService } from 'app/entities/a-user/service/a-user.service';

import { DeliveryUpdateComponent } from './delivery-update.component';

describe('Delivery Management Update Component', () => {
  let comp: DeliveryUpdateComponent;
  let fixture: ComponentFixture<DeliveryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let deliveryService: DeliveryService;
  let aUserService: AUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DeliveryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DeliveryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DeliveryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    deliveryService = TestBed.inject(DeliveryService);
    aUserService = TestBed.inject(AUserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call AUser query and add missing value', () => {
      const delivery: IDelivery = { id: 456 };
      const creator: IAUser = { id: 55687 };
      delivery.creator = creator;

      const aUserCollection: IAUser[] = [{ id: 42744 }];
      jest.spyOn(aUserService, 'query').mockReturnValue(of(new HttpResponse({ body: aUserCollection })));
      const additionalAUsers = [creator];
      const expectedCollection: IAUser[] = [...additionalAUsers, ...aUserCollection];
      jest.spyOn(aUserService, 'addAUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ delivery });
      comp.ngOnInit();

      expect(aUserService.query).toHaveBeenCalled();
      expect(aUserService.addAUserToCollectionIfMissing).toHaveBeenCalledWith(aUserCollection, ...additionalAUsers);
      expect(comp.aUsersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const delivery: IDelivery = { id: 456 };
      const creator: IAUser = { id: 38455 };
      delivery.creator = creator;

      activatedRoute.data = of({ delivery });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(delivery));
      expect(comp.aUsersSharedCollection).toContain(creator);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Delivery>>();
      const delivery = { id: 123 };
      jest.spyOn(deliveryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ delivery });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: delivery }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(deliveryService.update).toHaveBeenCalledWith(delivery);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Delivery>>();
      const delivery = new Delivery();
      jest.spyOn(deliveryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ delivery });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: delivery }));
      saveSubject.complete();

      // THEN
      expect(deliveryService.create).toHaveBeenCalledWith(delivery);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Delivery>>();
      const delivery = { id: 123 };
      jest.spyOn(deliveryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ delivery });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(deliveryService.update).toHaveBeenCalledWith(delivery);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackAUserById', () => {
      it('Should return tracked AUser primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
