import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IDelivery, Delivery } from '../delivery.model';
import { DeliveryService } from '../service/delivery.service';
import { IAUser } from 'app/entities/a-user/a-user.model';
import { AUserService } from 'app/entities/a-user/service/a-user.service';

@Component({
  selector: 'jhi-delivery-update',
  templateUrl: './delivery-update.component.html',
})
export class DeliveryUpdateComponent implements OnInit {
  isSaving = false;

  aUsersSharedCollection: IAUser[] = [];

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    streetAddress: [],
    postalCode: [],
    city: [],
    stateProvince: [],
    paid: [],
    tripPrice: [null, [Validators.min(0)]],
    creator: [],
  });

  constructor(
    protected deliveryService: DeliveryService,
    protected aUserService: AUserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ delivery }) => {
      this.updateForm(delivery);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const delivery = this.createFromForm();
    if (delivery.id !== undefined) {
      this.subscribeToSaveResponse(this.deliveryService.update(delivery));
    } else {
      this.subscribeToSaveResponse(this.deliveryService.create(delivery));
    }
  }

  trackAUserById(_index: number, item: IAUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDelivery>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(delivery: IDelivery): void {
    this.editForm.patchValue({
      id: delivery.id,
      streetAddress: delivery.streetAddress,
      postalCode: delivery.postalCode,
      city: delivery.city,
      stateProvince: delivery.stateProvince,
      paid: delivery.paid,
      tripPrice: delivery.tripPrice,
      creator: delivery.creator,
    });

    this.aUsersSharedCollection = this.aUserService.addAUserToCollectionIfMissing(this.aUsersSharedCollection, delivery.creator);
  }

  protected loadRelationshipsOptions(): void {
    this.aUserService
      .query()
      .pipe(map((res: HttpResponse<IAUser[]>) => res.body ?? []))
      .pipe(map((aUsers: IAUser[]) => this.aUserService.addAUserToCollectionIfMissing(aUsers, this.editForm.get('creator')!.value)))
      .subscribe((aUsers: IAUser[]) => (this.aUsersSharedCollection = aUsers));
  }

  protected createFromForm(): IDelivery {
    return {
      ...new Delivery(),
      id: this.editForm.get(['id'])!.value,
      streetAddress: this.editForm.get(['streetAddress'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      city: this.editForm.get(['city'])!.value,
      stateProvince: this.editForm.get(['stateProvince'])!.value,
      paid: this.editForm.get(['paid'])!.value,
      tripPrice: this.editForm.get(['tripPrice'])!.value,
      creator: this.editForm.get(['creator'])!.value,
    };
  }
}
