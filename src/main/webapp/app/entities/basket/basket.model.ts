import dayjs from 'dayjs/esm';
import { IDelivery } from 'app/entities/delivery/delivery.model';
import { IAUser } from 'app/entities/a-user/a-user.model';
import { IProducts } from 'app/entities/products/products.model';

export interface IBasket {
  id?: number;
  creationDate?: dayjs.Dayjs | null;
  price?: number | null;
  delivery?: IDelivery | null;
  bies?: IAUser[] | null;
  choosen?: IProducts | null;
}

export class Basket implements IBasket {
  constructor(
    public id?: number,
    public creationDate?: dayjs.Dayjs | null,
    public price?: number | null,
    public delivery?: IDelivery | null,
    public bies?: IAUser[] | null,
    public choosen?: IProducts | null
  ) {}
}

export function getBasketIdentifier(basket: IBasket): number | undefined {
  return basket.id;
}
