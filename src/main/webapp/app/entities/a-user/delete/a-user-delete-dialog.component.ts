import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAUser } from '../a-user.model';
import { AUserService } from '../service/a-user.service';

@Component({
  templateUrl: './a-user-delete-dialog.component.html',
})
export class AUserDeleteDialogComponent {
  aUser?: IAUser;

  constructor(protected aUserService: AUserService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.aUserService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
